package com.cinema.service;

import java.util.function.Predicate;

public class SeatPredicate implements Predicate<Seat> {
    private Seat seat;

    public SeatPredicate(Seat seat) {
        this.seat = seat;
    }

    @Override
    public boolean test(Seat s) {
        return s.getRow()==seat.getRow() && s.getColumn() == seat.getColumn();
    }
}
