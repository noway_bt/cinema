package com.cinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class SeatService {
    private Seats seats;
    private Map<String,Seat> mapOfSoldSeats;

    public SeatService(@Autowired Seats seats) {
        this.seats = seats;
        mapOfSoldSeats = new HashMap<>();
    }

    public Seats getSeats() {
        return seats;
    }

    public Ticket purchaseTicket(Seat seat) {
        validateSeat(seat);
        seats.getAvailableSeats().removeIf(new SeatPredicate(seat));
        Ticket newTicket = new Ticket(generateToken(), calculateSeatPrice(seat));
        mapOfSoldSeats.put(newTicket.getToken(),newTicket.getSeat());
        return newTicket;
    }

    public Map calculateStat() {
        Map<String, Integer> cinemaStat = new HashMap<>();
        cinemaStat.put("current_income", this.calculateIncome());
        cinemaStat.put("number_of_available_seats",seats.getAvailableSeats().size());
        cinemaStat.put("number_of_purchased_tickets", mapOfSoldSeats.size());
        return cinemaStat;
    }

    private int calculateIncome() {
        int income = 0;
        for (Seat seat : mapOfSoldSeats.values()) {
            income += seat.getPrice();
        }
        return income;
    }

    public Seat returnedSeat(Ticket ticket) {
        if(!mapOfSoldSeats.containsKey(ticket.getToken())){
            throw new IllegalArgumentException("Wrong token!");
        }
        Seat returnedSeat = mapOfSoldSeats.get(ticket.getToken());
        mapOfSoldSeats.remove(ticket.getToken());
        seats.getAvailableSeats().add(returnedSeat);
        return returnedSeat;
    }

    private Seat calculateSeatPrice(Seat seat) {
        if (seat.getRow() <= 4) {
            return new Seat(seat.getRow(), seat.getColumn(), 10);
        } else {
            return new Seat(seat.getRow(), seat.getColumn(), 8);
        }
    }

    private String generateToken() {
        return UUID.randomUUID().toString();
    }

    private void validateSeat(Seat seat) {
        if (seat.getRow() > seats.getTotalRows() || seat.getRow() <= 0) {
            throw new IllegalArgumentException("The number of a row or a column is out of bounds!");
        }
        if (seat.getColumn() > seats.getTotalColumns() || seat.getColumn() <= 0) {
            throw new IllegalArgumentException("The number of a row or a column is out of bounds!");
        }
        if (!available(seat)) {
            throw new IllegalArgumentException("The ticket has been already purchased!");
        }
    }

    private boolean available(Seat seat) {
        return seats.getAvailableSeats().stream().anyMatch(new SeatPredicate(seat));
    }
}
