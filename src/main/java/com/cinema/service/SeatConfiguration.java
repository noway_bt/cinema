package com.cinema.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class SeatConfiguration {

    @Value("${cinema.rows}")
    private int rows;

    @Bean
    public int getRows() {
        return rows;
    }

    @Value("${cinema.columns}")
    private int columns;

    @Bean
    public int getColumns() {
        return columns;
    }

    @Bean
    public Seats generateSeats(@Qualifier("getRows") int rows, @Qualifier("getColumns") int columns) {
        List<Seat> seatsList = new ArrayList<>();
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= columns; j++) {
                if (i <= 4) {
                    seatsList.add(new Seat(i, j, 10));
                } else {
                    seatsList.add(new Seat(i, j, 8));
                }
            }
        }
        return new Seats(rows, columns, seatsList);
    }
}
