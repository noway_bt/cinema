package com.cinema.service;

public class Ticket {
    String token;
    Seat seat;

    public Ticket(String token, Seat ticket) {
        this.token = token;
        this.seat = ticket;
    }

    public Seat getSeat() {
        return seat;
    }

    public void setSeat(Seat seat) {
        this.seat = seat;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
