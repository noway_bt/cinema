package com.cinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

@RestController
public class SeatController {
    private SeatService seatService;

    public SeatController(@Autowired SeatService seatService) {
        this.seatService = seatService;
    }

    @GetMapping("/seats")
    public Seats seats() {
        return seatService.getSeats();
    }

    @PostMapping("/purchase")
    public Ticket purchaseSeat(@RequestBody Seat seat) {
        return seatService.purchaseTicket(seat);
    }

    @PostMapping("/return")
    public Map<String,Seat> returnTicket(@RequestBody Ticket ticket) {
        return Map.of("returned_ticket",seatService.returnedSeat(ticket));
    }

    @PostMapping("/stats")
    public Map<String,Integer> returnStats(@RequestParam (required = false) String password) {
        if(!"super_secret".equals(password)) {
            throw new UnauthorizedException("The password is wrong!");
        }
        return seatService.calculateStat();
    }

}
